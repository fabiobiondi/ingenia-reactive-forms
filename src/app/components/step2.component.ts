import { Component, forwardRef, Input, OnInit } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormBuilder,
  FormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-step2',
  template: `
    <div [formGroup]="form">

      <h2>
        <i class="fa fa-check alert-success"
           *ngIf="form.valid"></i>
        Step 2: Your Info
      </h2>

      <div>

        <div class="form-group">
          <input type="text"
                 formControlName="brand"
                 class="form-control"
                 placeholder="Brand"
                 [ngClass]="{'is-invalid': form.get('brand').invalid}"
          >
        </div>

        <div class="form-group">
          <input type="text"
                 formControlName="model"
                 class="form-control"
                 placeholder="Model"
                 [ngClass]="{'is-invalid': form.get('model').invalid}">
        </div>
      </div>
    </div>
  `,
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => Step2Component), multi: true },
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => Step2Component), multi: true },
  ]
})
export class Step2Component implements ControlValueAccessor, Validator {
  onTouched: () => void;
  onChanged: (val) => void;
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = fb.group({
      brand: ['', Validators.required],
      model: ['', Validators.required],
    });
  }
  registerOnChange(fn: any): void {
    this.form.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: CarInfo): void {
    this.form.setValue(obj);
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return this.form.invalid ? {
      missingFields: true,
    } : null;
  }

}

interface CarInfo {
  brand: string;
  model: string;
}
