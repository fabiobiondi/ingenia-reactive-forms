import { Component, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-star',
  template: `
    
    <i
      *ngFor="let star of [null,null,null,null,null]; let i = index"
      class="fa"
      [ngClass]="{
        'fa-star': i+1 <= value,
        'fa-star-o': i+1 > value
      }"
      (mouseover)="onChangeHandler(i+1)"
    ></i>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => StarComponent), multi: true }
  ]
})
export class StarComponent implements ControlValueAccessor {
  value: number;
  onChange: (rate: number) => void;
  onTouch: () => void;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(rate: number): void {
    this.value = rate;
  }

  onChangeHandler(rate: number): void {
    this.value = rate;
    this.onChange(rate);
  }
}
