import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-color-picker',
  template: `
   
    <span
      *ngFor="let color of colors"
      class="color-cell"
      [style.backgroundColor]="color"
      [style.borderWidth.px]="color === value ? 5:1"
      (click)="changeColorHandler(color)"
      [style.opacity]="isDisabled ? 0.4 : 1"
      [style.pointer-events]="isDisabled ? 'none' : null"
    >
      
    </span>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => ColorPickerComponent), multi: true }
  ],
  styles: [`
    .color-cell {
      display: inline-block;
      width: 50px; height: 50px;
      border: 1px solid black;
      margin-right: 2px;
    }
  `]
})
export class ColorPickerComponent implements ControlValueAccessor {
  @Input() colors = [];
  onChange: (rate: string) => void;
  onTouch: () => void;
  value: string;
  isDisabled: boolean;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(color: string): void {
    this.value = color;
  }


  changeColorHandler(color: string): void {
    this.onTouch();
    this.value = color;
    this.onChange(this.value);

  }
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
