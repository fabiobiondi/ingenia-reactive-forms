import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AsyncValidatorFn, FormControl } from '@angular/forms';
import { of, timer } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root'})
export class UserValidatorService {

  constructor(private http: HttpClient) {}

  uniqueName(): AsyncValidatorFn {
    return (c: FormControl) => {
      return timer(1000)
        .pipe(
          switchMap(() => this.http.get<any[]>(`https://jsonplaceholder.typicode.com/users?username=${c.value}`)),
          map(res => res.length ? { exists: true} : null)
        );
    };
  }
}
