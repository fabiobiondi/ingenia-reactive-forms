import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-weather',
  template: `
    <h1>{{meteo?.main.temp}}</h1>
  `,
})
export class WeatherComponent {
  @Input() meteo: any;
}
