import { Component, forwardRef, Injector, Input, OnInit } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, NgControl, ValidationErrors,
  Validator,
  Validators
} from '@angular/forms';

const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;

@Component({
  selector: 'app-my-simple-input',
  template: `
    
    <div class="form-group">
      <small>{{label}}</small>
      <input
        class="form-control"
        type="text" 
        [formControl]="simpleInput"
        [ngClass]="{'is-invalid': ngControl.invalid}"
      >
    </div>
  `,
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => MySimpleInputComponent), multi: true },
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MySimpleInputComponent), multi: true }
  ],
})
export class MySimpleInputComponent implements OnInit, ControlValueAccessor, Validator {
  @Input() label: string;
  @Input() alphaNumeric: boolean;
  onChange: (text: string) => void;
  onTouch: () => void;
  ngControl: NgControl;

  simpleInput: FormControl = new FormControl();

  constructor(private inj: Injector) { }

  ngOnInit(): void {
    this.ngControl = this.inj.get(NgControl);
  }
  registerOnChange(fn: any): void {
    this.simpleInput.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(text: string): void {
    this.simpleInput.setValue(text)
  }

  validate(c: AbstractControl): ValidationErrors | null {
    if (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) {
      return {
        alphaNumeric: true
      };
    }
  }
/*
  validate(control: AbstractControl): ValidationErrors | null {
    return null;
  }*/

}
