import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-step1',
  template: `
    <div [formGroup]="group">

      <h2>
        <i class="fa fa-check alert-success"
           *ngIf="group.valid"></i>
        Step 1: Your car
      </h2>

      <div>

        <div class="form-group">
          <input type="text"
                 formControlName="brand"
                 class="form-control"
                 placeholder="Brand"
                 [ngClass]="{'is-invalid': group.get('brand').invalid}"
          >
        </div>

        <div class="form-group">
          <input type="text"
                 formControlName="model"
                 class="form-control"
                 placeholder="Model"
                 [ngClass]="{'is-invalid': group.get('model').invalid}">
        </div>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class Step1Component {
  @Input() group;

}
