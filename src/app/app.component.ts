import { AfterViewInit, Component, ElementRef, Injectable, ViewChild } from '@angular/core';
import { forkJoin, fromEvent, Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, filter, map, mergeAll, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { AbstractControl, FormBuilder, FormControl, FormGroup, NgForm, ValidationErrors, Validators } from '@angular/forms';
import { UserValidatorService } from './components/user-validator.service';

@Component({
  selector: 'app-root',
  template: `
    <form  [formGroup]="form" (submit)="sendHandler()">
     
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1">
            <i class="fa fa-spinner fa-spin fa-fw" *ngIf="form.get('name').pending"></i>
            <i class="fa fa-check" *ngIf="form.get('name').valid"></i>
            <i class="fa fa-exclamation-circle" *ngIf="form.get('name').invalid"></i>
          </span>
        </div>
        <input
          type="text" class="form-control"
          formControlName="name"
          placeholder="name"
        >
      </div>
      <pre>valid {{form.get('name').valid | json}}</pre>
      <pre>invalid {{form.get('name').invalid | json}}</pre>
      <pre>pending {{form.get('name').pending | json}}</pre>
      
      <app-my-simple-input
        label="Surname"
        [alphaNumeric]="true"
        formControlName="surname"></app-my-simple-input>
      <div *ngIf="form.get('surname').invalid">invalid</div>
      <br>

      <app-star formControlName="rate"></app-star>
      <br>
      <app-color-picker 
        formControlName="bg" 
        [colors]="['blue', 'orange', 'pink']"
      ></app-color-picker>
      <br>

      <app-step1 [group]="form.get('step1')"></app-step1>

      <!--&ndash;&gt; {{form.get('step2').errors | json}}-->
      <app-step2 formControlName="step2"></app-step2>
      
      
      <h4>Passwords</h4>
      <div formGroupName="passwords">
        <input type="text" formControlName="password1">
        <input type="text" formControlName="password2">
        {{form.get('passwords').get('password1').errors |  json}}
        {{form.get('passwords').get('password2').errors |  json}}
      </div>
      
      <button type="submit" [disabled]="form.invalid || form.pending">Send</button>
    </form>
    
    <pre>{{form.value | json}}</pre>
    
  `,
})
export class AppComponent {
  form: FormGroup;
  // form array, form multistep

  constructor(private fb: FormBuilder, private userValidator: UserValidatorService) {

    this.form = fb.group({
      name: ['a', [Validators.required], userValidator.uniqueName()],
      surname: ['b', Validators.required],
      rate: 3,
      bg: [null, Validators.required],
      step1: fb.group({
        brand: ['', Validators.required],
        model: ['', Validators.required],
      }),
      step2: {
        brand: 'A',
        model: 'B'
      },
      passwords: fb.group({
        password1: ['', [Validators.required, Validators.minLength(3)]],
        password2: ['', [Validators.required, Validators.minLength(3)]],
      }, { validators: passwordMatch('password1', 'password2')})
    });

    setTimeout(() => {
      // this.form.get('bg').disable();
      // this.form.setValue({ name: 'pippo', bg: 'orange', surname: 'ciccio', rate: 1})
      this.form.get('step2').setValue({ brand: 'XX', model: 'YY'})
    }, 3000)
  }


  sendHandler(): void {
    console.log(this.form.value)
  }

  submitHandler(f: NgForm): void {
    console.log(f.value)
  }
}

export function passwordMatch(p1: string, p2: string): any {
  return (group) => {
    const pass1 = group.get(p1);
    const pass2 = group.get(p2);

    if (pass1.value !== pass2.value) {
      pass2.setErrors({ noMatch: true })
    } else {
      pass2.setErrors(null);
      pass2.setErrors(pass2.errors ? { ...pass2.errors } : null)
    }

  }
}


// =====

const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;

export function alphaNumericValidator(c: AbstractControl, totalLength: number): ValidationErrors | null {
  if (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) {
    return { alphaNumeric: true  };
  }

  if (c.value && c.value.length !== totalLength) {
    const diff = totalLength - c.value.length
    return {
      totalLength: true,
      lengthh: c.value.length,
      diff
    };
  }
  return null;
}
