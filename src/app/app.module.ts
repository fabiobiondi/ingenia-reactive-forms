import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { WeatherComponent } from './components/weather.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MySimpleInputComponent } from './components/my-simple-input.component';
import { StarComponent } from './components/star.component';
import { ColorPickerComponent } from './components/color-picker.component';
import { Step1Component } from './components/step1.component';
import { Step2Component } from './components/step2.component';

@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent,
    MySimpleInputComponent,
    StarComponent,
    ColorPickerComponent,
    Step1Component,
    Step2Component
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
